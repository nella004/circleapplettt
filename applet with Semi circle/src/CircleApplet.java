import java.awt.Graphics;

import javax.swing.JApplet;

public class CircleApplet extends JApplet {

	// TODO Auto-generated method stub

	public void paint(Graphics canvas) {

		canvas.drawOval(154, 104, 80, 80);
		canvas.fillOval(170, 120, 50, 50);
		canvas.drawArc(147, 12, 100, 90, 180, 180);
		canvas.drawArc(147, 186, 100, 90, 0, 180);
		canvas.drawArc(236, 100, 100, 90, 90, 180);
		canvas.drawArc(52, 100, 100, 90, 270, 180);
		// canvas.drawArc(x, y, width, height, startAngle, arcAngle);
	}

}